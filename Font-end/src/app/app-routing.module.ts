import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShowAllServiceComponent } from './pages/service/show-all-service/show-all-service.component';
import { ShowHomeComponent } from './pages/home/show-home/show-home.component';
import { ShowAllBarberComponent } from './pages/barber/show-all-barber/show-all-barber.component';
import { ShowAllNewsComponent } from './pages/information/show-all-news/show-all-news.component';
import { ShowAllPromotionComponent } from './pages/information/show-all-promotion/show-all-promotion.component';
import { ShowAllBranchComponent } from './pages/branch/show-all-branch/show-all-branch.component';
import { AddUsersComponent } from './pages/users/add-users/add-users.component';

const routes: Routes = [
{path:"",component:ShowHomeComponent}, 
{path:"show-all-service",component:ShowAllServiceComponent},
{path:"show-all-barber",component:ShowAllBarberComponent}, 
{path:"show-all-news",component:ShowAllNewsComponent}, 
{path:"show-all-promotion",component:ShowAllPromotionComponent},
{path:"show-all-branch",component:ShowAllBranchComponent},
{path:"add-user",component:AddUsersComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
