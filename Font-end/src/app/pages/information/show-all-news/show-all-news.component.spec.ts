import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowAllNewsComponent } from './show-all-news.component';

describe('ShowAllNewsComponent', () => {
  let component: ShowAllNewsComponent;
  let fixture: ComponentFixture<ShowAllNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowAllNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowAllNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
