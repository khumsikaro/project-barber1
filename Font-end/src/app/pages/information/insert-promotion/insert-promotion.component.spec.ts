import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertPromotionComponent } from './insert-promotion.component';

describe('InsertPromotionComponent', () => {
  let component: InsertPromotionComponent;
  let fixture: ComponentFixture<InsertPromotionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsertPromotionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertPromotionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
