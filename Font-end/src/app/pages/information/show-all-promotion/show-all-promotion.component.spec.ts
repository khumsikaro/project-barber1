import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowAllPromotionComponent } from './show-all-promotion.component';

describe('ShowAllPromotionComponent', () => {
  let component: ShowAllPromotionComponent;
  let fixture: ComponentFixture<ShowAllPromotionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowAllPromotionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowAllPromotionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
