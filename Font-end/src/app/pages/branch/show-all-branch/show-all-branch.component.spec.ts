import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowAllBranchComponent } from './show-all-branch.component';

describe('ShowAllBranchComponent', () => {
  let component: ShowAllBranchComponent;
  let fixture: ComponentFixture<ShowAllBranchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowAllBranchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowAllBranchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
