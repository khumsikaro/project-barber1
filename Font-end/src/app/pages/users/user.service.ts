import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  Url: string = 'http://192.168.137.169:3000/';
  constructor(private http: HttpClient) { }

  public addUser() {
    return this.http.get(this.Url + "addUser")
  }
  
}

