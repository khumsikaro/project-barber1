import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertBarberNorComponent } from './insert-barber-nor.component';

describe('InsertBarberNorComponent', () => {
  let component: InsertBarberNorComponent;
  let fixture: ComponentFixture<InsertBarberNorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsertBarberNorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertBarberNorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
