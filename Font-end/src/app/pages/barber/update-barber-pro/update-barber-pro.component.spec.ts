import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateBarberProComponent } from './update-barber-pro.component';

describe('UpdateBarberProComponent', () => {
  let component: UpdateBarberProComponent;
  let fixture: ComponentFixture<UpdateBarberProComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateBarberProComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateBarberProComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
