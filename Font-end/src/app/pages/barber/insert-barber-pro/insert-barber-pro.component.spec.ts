import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertBarberProComponent } from './insert-barber-pro.component';

describe('InsertBarberProComponent', () => {
  let component: InsertBarberProComponent;
  let fixture: ComponentFixture<InsertBarberProComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsertBarberProComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertBarberProComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
