import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateBarberNorComponent } from './update-barber-nor.component';

describe('UpdateBarberNorComponent', () => {
  let component: UpdateBarberNorComponent;
  let fixture: ComponentFixture<UpdateBarberNorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateBarberNorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateBarberNorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
