import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowAllBarberComponent } from './show-all-barber.component';

describe('ShowAllBarberComponent', () => {
  let component: ShowAllBarberComponent;
  let fixture: ComponentFixture<ShowAllBarberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowAllBarberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowAllBarberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
