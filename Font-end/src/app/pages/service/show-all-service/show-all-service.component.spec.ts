import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowAllServiceComponent } from './show-all-service.component';

describe('ShowAllServiceComponent', () => {
  let component: ShowAllServiceComponent;
  let fixture: ComponentFixture<ShowAllServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowAllServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowAllServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
