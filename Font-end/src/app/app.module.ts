import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IconsProviderModule } from './icons-provider.module';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { InsertNewsComponent } from './pages/information/insert-news/insert-news.component';
import { UpdateNewsComponent } from './pages/information/update-news/update-news.component';
import { ShowAllNewsComponent } from './pages/information/show-all-news/show-all-news.component';
import { ShowNewsComponent } from './pages/information/show-news/show-news.component';
import { ShowPromotionComponent } from './pages/information/show-promotion/show-promotion.component';
import { ShowAllPromotionComponent } from './pages/information/show-all-promotion/show-all-promotion.component';
import { UpdatePromotionComponent } from './pages/information/update-promotion/update-promotion.component';
import { InsertPromotionComponent } from './pages/information/insert-promotion/insert-promotion.component';
import { InsertServiceComponent } from './pages/service/insert-service/insert-service.component';
import { UpdateServiceComponent } from './pages/service/update-service/update-service.component';
import { ShowServiceComponent } from './pages/service/show-service/show-service.component';
import { ShowAllServiceComponent } from './pages/service/show-all-service/show-all-service.component';
import { ShowAllBarberComponent } from './pages/barber/show-all-barber/show-all-barber.component';
import { ShowBarberComponent } from './pages/barber/show-barber/show-barber.component';
import { InsertBarberNorComponent } from './pages/barber/insert-barber-nor/insert-barber-nor.component';
import { InsertBarberProComponent } from './pages/barber/insert-barber-pro/insert-barber-pro.component';
import { UpdateBarberProComponent } from './pages/barber/update-barber-pro/update-barber-pro.component';
import { UpdateBarberNorComponent } from './pages/barber/update-barber-nor/update-barber-nor.component';
import { from } from 'rxjs';
import { Observable, Observer } from 'rxjs';
import { ShowHomeComponent } from './pages/home/show-home/show-home.component';
import { ShowAllBranchComponent } from './pages/branch/show-all-branch/show-all-branch.component';
import { ShowBranchComponent } from './pages/branch/show-branch/show-branch.component';
import { InsertBranchComponent } from './pages/branch/insert-branch/insert-branch.component';
import { UpdateBranchComponent } from './pages/branch/update-branch/update-branch.component';

import{ServiceService} from './pages/service/service.service';
import { AddUsersComponent } from './pages/users/add-users/add-users.component';




registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    InsertNewsComponent,
    UpdateNewsComponent,
    ShowAllNewsComponent,
    ShowNewsComponent,
    ShowPromotionComponent,
    ShowAllPromotionComponent,
    UpdatePromotionComponent,
    InsertPromotionComponent,
    InsertServiceComponent,
    UpdateServiceComponent,
    ShowServiceComponent,
    ShowAllServiceComponent,
    ShowAllBarberComponent,
    ShowBarberComponent,
    InsertBarberNorComponent,
    InsertBarberProComponent,
    UpdateBarberProComponent,
    UpdateBarberNorComponent,
    ShowHomeComponent,
    ShowAllBranchComponent,
    ShowBranchComponent,
    InsertBranchComponent,
    UpdateBranchComponent,
    AddUsersComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    IconsProviderModule,
    NgZorroAntdModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NzGridModule,
    NzDrawerModule,
    NzModalModule,
    ReactiveFormsModule,

    
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US,}, ServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }


